FROM ubuntu:18.04

ARG SERVICE_NAME=dliver-image-processor

ENV GRAPHICSMAGICK_VERSION=1.3.31
ENV GRAPHICSMAGICK_FOLDER=/opt/graphicsmagick
ENV DECOMPRESSED_FOLDER=$GRAPHICHMAGICK_FOLDER/src

RUN apt-get update && apt-get install -y \
  libjpeg-turbo8-dev \
  libltdl-dev \
  libpng-dev \
  libtool \
  libxml2-dev \
  libwebp-dev \
  libtiff-dev \
  wget \
  zlib1g-dev

# Download GraphicsMagick from SourceForge (recommended)
RUN wget --directory /tmp https://downloads.sourceforge.net/project/graphicsmagick/graphicsmagick/$GRAPHICSMAGICK_VERSION/GraphicsMagick-$GRAPHICSMAGICK_VERSION.tar.gz

# Decompress package into /opt/graphicsmagick/src
RUN mkdir --parents $DECOMPRESSED_FOLDER \
  && tar zxvf /tmp/GraphicsMagick-$GRAPHICSMAGICK_VERSION.tar.gz --directory $DECOMPRESSED_FOLDER

# Build binaries
RUN cd $DECOMPRESSED_FOLDER/GraphicsMagick-$GRAPHICSMAGICK_VERSION \
  && ./configure \
    --disable-static \
    --enable-shared \
    --prefix=$GRAPHICSMAGICK_FOLDER/GraphicsMagick-$GRAPHICSMAGICK_VERSION \
    --with-jp2=yes \
    --with-jpeg=yes \
    --with-modules \
    --with-threads \
    --with-tiff=yes \
    --with-xml=yes \
    --with-webp=yes \
  && make \
  && make install

RUN ln -s "$GRAPHICSMAGICK_FOLDER/GraphicsMagick-$GRAPHICSMAGICK_VERSION/bin/gm" "/usr/bin/gm"

ENV PATH="/usr/bin/gm:${PATH}"
